using System.Security.Cryptography;
using dotnetcore.Infrastructure.RandomGenerator.Base;

namespace dotnetcore.Infrastructure.RandomGenerator;

/// <summary>
/// Используем криптографически стойкий генератор случайных чисел.
/// </summary>
public class RandomCryptographicNumber : RandomNumber
{
    public RandomCryptographicNumber(int upperBound) : base(upperBound)
    {
    }

    public override int Get() => RandomNumberGenerator.GetInt32(UpperBound);
}