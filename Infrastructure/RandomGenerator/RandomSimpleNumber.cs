using System;
using dotnetcore.Infrastructure.RandomGenerator.Base;

namespace dotnetcore.Infrastructure.RandomGenerator;

/// <summary>
/// Используем простой генератор псевдослучайных чисел.
/// </summary>
public class RandomSimpleNumber : RandomNumber
{
    private Random _random;

    public RandomSimpleNumber(int upperBound) : base(upperBound) 
        => _random = new Random();

    public override int Get() => _random.Next(UpperBound);
}