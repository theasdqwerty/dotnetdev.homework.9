using System;
using dotnetcore.Abstactions;

namespace dotnetcore.Infrastructure.IO;

public class OutputConsoleMessage : IOutputMessage
{
    public void Output(string message) => Console.WriteLine(message);
}