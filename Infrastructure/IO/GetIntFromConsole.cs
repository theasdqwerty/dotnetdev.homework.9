using System;
using dotnetcore.Abstactions;

namespace dotnetcore.Infrastructure.IO;

/*
 * Ответственность данного класса получить с консоли вводимое пользователем число и вернуть его.
 * У класса только одна ответственность получить число с консоли,
 * это и является причиной его существования. Принцип единой ответственности ORP.
 * 
 * Класс реализует интерфейс IGetInputInt, если понадобиться получить число из другого
 * источника, нужно будет просто расширить функционал создать еще один класс который будет
 * реализовать данный интерфейс. Принцип OCP.
 * 
 */

/// <summary>
/// Получить ввод числа с окна терминала.
/// </summary>
public class GetIntFromConsole : IGetInputInt
{
    public int Get()
    {
        int number;
        if (int.TryParse(Console.ReadLine(), out number))
            return number;

        return -1;
    }
}