using dotnetcore.Abstactions;

namespace dotnetcore.Infrastructure;

public class Game
{
    private IGetRandomInt _getRandomInt;
    private IGetInputInt _getInputInt;
    private IOutputMessage _outputMessage;
    private int _numberOfAttempts;

    public Game(Settings settings)
    {
        _getRandomInt = settings.GetRandomInt;
        _getInputInt = settings.GetInputInt;
        _outputMessage = settings.OutputMessage;
        _numberOfAttempts = settings.NumberOfAttempts;
    }

    public void Play()
    {
        var number = _getRandomInt.Get();
        
        for (int i = 0; i < _numberOfAttempts; i++)
        {
            _outputMessage.Output($"Попытка №{i + 1}.\nВведите число:");
            
            var compareNumber = _getInputInt.Get();
            if (compareNumber == -1)
                continue;
            
            if (compareNumber > number)
            {
                _outputMessage.Output("Число меньше.\n");
                continue;
            }
         
            if (compareNumber < number)
            {
                _outputMessage.Output("Число больше.\n");
                continue;
            }
         
            if (compareNumber == number)
            {
                _outputMessage.Output("Ура. Число найдено!\n");
                return;
            }               
        }
        
        _outputMessage.Output("Увы. Число не найдено.\n");
    }

}