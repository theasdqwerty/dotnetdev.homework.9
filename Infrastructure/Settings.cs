using dotnetcore.Abstactions;

namespace dotnetcore.Infrastructure;

public class Settings
{
    public IGetRandomInt GetRandomInt { get; }
    public IGetInputInt GetInputInt { get; }
    public IOutputMessage OutputMessage { get; }
    public int NumberOfAttempts { get; }

    public Settings(IGetRandomInt getRandomInt, IGetInputInt getInputInt, IOutputMessage outputMessage, int numberOfAttempts)
    {
        GetRandomInt = getRandomInt;
        GetInputInt = getInputInt;
        NumberOfAttempts = numberOfAttempts;
        OutputMessage = outputMessage;
    }
}