﻿using dotnetcore.Infrastructure;
using dotnetcore.Infrastructure.IO;
using dotnetcore.Infrastructure.RandomGenerator;

namespace dotnetcore
{
    public class Program
    {
        private static void Main()
        {
            var settings = new Settings(new RandomCryptographicNumber(100), new GetIntFromConsole(), 
                new OutputConsoleMessage(), 100);

            var game = new Game(settings);
            game.Play();
        }
    }
}
