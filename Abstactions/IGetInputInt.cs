namespace dotnetcore.Abstactions;

public interface IGetInputInt
{
    public int Get();
}