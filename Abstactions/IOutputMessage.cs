namespace dotnetcore.Abstactions;

public interface IOutputMessage
{
    public void Output(string message);
}