namespace dotnetcore.Abstactions;

public interface IGetRandomInt
{
    public int Get();
}